class Adjacency
{
    constructor(door, room)
    {
        this.door = door;
        this.room = room;
        this._direction = null;
    }

    get direction()
    {
        if ( this._direction === null )
        {
            if ( this.door.x1 < this.door.x2 )
                this._direction = this.room.x > this.door.x1 ? Directions.Right : Directions.Left;
            else
                this._direction = this.room.y > this.door.y1 ? Directions.Down : Directions.Up;
        }
        return this._direction;
    }
}

class Door
{
    constructor(x1, y1, x2, y2, classname="")
    {
        this.x1 = x1;
        this.y1 = y1;
        this.x2 = x2;
        this.y2 = y2;
        this.classname = classname;
        this.group = null;
    }

    to_dom(cell_size)
    {
        let div = make_rect(
            this.x1 * cell_size + cell_size / 4,
            this.y1 * cell_size + cell_size / 4,
            (this.x2 - this.x1 + 1) * cell_size - cell_size / 2,
            (this.y2 - this.y1 + 1) * cell_size - cell_size / 2,
            this.classname
        );

        if ( this.group !== null )
        {
            div.appendChild(document.createTextNode(this.group));
        }

        return div;
    }
}


class MazeArea
{
    constructor(tag)
    {
        this.rooms = [];
        this.tag = tag;
    }

    neighbours()
    {
        let neigh = new Set();
        for ( let room of this.rooms )
            for ( let adj of room.adjacency.map(a => a.room).filter(r => r.area !== this) )
                neigh.add(adj.area);
        return [...neigh];
    }

    smallest_neighbour()
    {
        let neigh = this.neighbours();
        if ( neigh.length === 0 )
            return null;
        return neigh.sort((a, b) => a.rooms.length - b.rooms.length)[0];
    }

    absorb(other)
    {
        // not perfect if multiple small areas are joined together
        if ( this.rooms.length < other.rooms.length )
            this.tag = other.tag;

        this.rooms = this.rooms.concat(other.rooms);
        for ( let room of other.rooms )
            room.area = this;
    }
}

class RoomEnvironment
{
    constructor(falloff=0.8, seeds=-1, seed_probability=0.1)
    {
        this.rooms = [];
        this.falloff = falloff;
        this.seeds = seeds;
        this.seed_probability = seed_probability;
        this.seeding = false;
    }

    get_probability(room)
    {
        this.seeding = false;
        if ( room.environment )
            return 0;

        let prob = this._on_get_probability(room);
        if ( prob < 0 && this.seeds != 0 )
        {
            this.seeding = true;
            prob = this.seed_probability;
        }

        return Math.max(0, prob) * this.falloff ** this.rooms.length;
    }

    get name()
    {
        return "env";
    }

    _on_get_probability(room)
    {
        for ( let adj of room.adjacency )
        {
            if ( adj.room.environment_name == this.name )
                return 1;
        }
        return -1;
    }

    add_room(room)
    {
        this.rooms.push(room);
        if ( this.seeding && this.seeds > 0 )
        {
            this.seeds -= 1;
            this.seeding = false;
        }
    }

    get area_tag()
    {
        return this.name;
    }
}
