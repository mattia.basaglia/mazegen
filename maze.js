function make_rect(x, y, w, h, classname)
{
    var div = document.createElement("div");
    div.style.position = "absolute";
    div.style.left = x + "px";
    div.style.top = y + "px";
    div.style.width = w + "px";
    div.style.height = h + "px";
    div.setAttribute("class", classname);
    return div;
}

const Directions = Object.freeze({
    Left: 1,
    Right: 2,
    Up: 4,
    Down: 8,
});

class Maze
{
    constructor(element, cell_size)
    {
        this.element = element;
        this.rooms = [];
        this.cell_size = cell_size;
        this.doors = [];
        this.max_room_w = 6;
        this.max_room_h = 6;
        this.min_room_w = 2;
        this.min_room_h = 2;
        this.min_x = 0;
        this.min_y = 0;
        this.max_x = this.element.clientWidth / cell_size;
        this.max_y = this.element.clientHeight / cell_size;
        this.expandable_rooms = [];
        this.expected_doors = 1.5;
        this.max_doors_per_room = 3;
        this.random = new Random(new SeedablePRNG());
    }

    add_room(room)
    {
        this.rooms.push(room);
        this.expandable_rooms.push(room);
    }

    render(show_cells=false)
    {
        var fc;
        while ( fc = this.element.firstChild )
            this.element.removeChild(fc);

        if ( show_cells )
        {
            var grid = this.element.appendChild(document.createElement("div"));
            grid.setAttribute("class", "grid");

            for ( var x = this.cell_size; x < this.max_x * this.cell_size; x += this.cell_size )
                grid.appendChild(make_rect(x-0.5, 0, 1, this.max_y * this.cell_size, "cell"));

            for ( var y = this.cell_size; y < this.max_y * this.cell_size; y += this.cell_size )
                grid.appendChild(make_rect(0, y-0.5, this.max_x * this.cell_size, 1, "cell"));
        }

        for ( var area of this.areas )
        {
            var room_cont = this.element.appendChild(document.createElement("div"));
            room_cont.setAttribute("class", "room");
            for ( var room of area.rooms )
            {
                room_cont.appendChild(room.to_dom(this.cell_size));
            }
        }

        var door_cont = this.element.appendChild(document.createElement("div"));
        door_cont.setAttribute("class", "door");
        for ( var door of this.doors )
        {
            door_cont.appendChild(door.to_dom(this.cell_size));
        }
    }

    expand_maze()
    {
        while ( this.expandable_rooms.length )
        {
            var index = this.random.rand_int(0, this.expandable_rooms.length-1);
            var room = this.expandable_rooms[index];
            if ( room.adjacency.length >= this.max_doors_per_room )
            {
                this.expandable_rooms.splice(index, 1);
                continue;
            }

            if ( this._seed_room(room) )
            {
                if ( room.adjacency.length >= this.max_doors_per_room )
                    this.expandable_rooms.splice(index, 1);
                return true;
            }
            else
            {
                this.expandable_rooms.splice(index, 1);
            }

        }
        return false;
    }

    fill_maze()
    {
        while ( this.expandable_rooms.length )
            this.expand_maze();
    }

    fill_doors()
    {
        var a_door_able = [];
        for ( var room of this.rooms )
        {
            if ( room.adjacency.length < this.max_doors_per_room )
                a_door_able.push(room);
        }

        var to_add = Math.floor(this.rooms.length * this.expected_doors) - this.doors.length;
        for ( var i = 0; i < to_add; i++ )
        {
            var index = this.random.rand_int(0, a_door_able.length - 1);
            var room = a_door_able[index];
            var valid_doors = [];
            var oth;


            for ( var x = 0; x < room.w; x++ )
            {
                if ( oth = this._valid_connection(room, room.x + x, room.y - 1) )
                    valid_doors.push([new Door(room.x + x, room.y - 1, room.x + x, room.y), oth]);

                if ( oth = this._valid_connection(room, room.x + x, room.y + room.h) )
                    valid_doors.push([new Door(room.x + x, room.y + room.h  - 1, room.x + x, room.y + room.h), oth]);
            }

            for ( var y = 0; y < room.h; y++ )
            {
                if ( oth = this._valid_connection(room, room.x - 1, room.y + y) )
                    valid_doors.push([new Door(room.x - 1, room.y + y, room.x, room.y + y), oth]);

                if ( oth = this._valid_connection(room, room.x + room.w, room.y + y) )
                    valid_doors.push([new Door(room.x + room.w - 1, room.y + y, room.x + room.w, room.y + y), oth]);
            }

            if ( valid_doors.length == 0 )
            {
                a_door_able.splice(index, 1);
                continue;
            }

            var [door, oth] = valid_doors[this.random.rand_int(0, valid_doors.length - 1)];
            this.doors.push(door);

            oth.adjacency.push(new Adjacency(door, room))
            if ( oth.adjacency.length >= this.max_doors_per_room )
                a_door_able.splice(a_door_able.indexOf(oth), 1);

            room.adjacency.push(new Adjacency(door, oth))
            if ( room.adjacency.length >= this.max_doors_per_room )
                a_door_able.splice(index, 1);

        }
    }

    seed(door)
    {
        return this._seed(door);
    }

    _seed(door)
    {
        var room1 = this.room_at(door.x1, door.y1);
        var room2 = this.room_at(door.x2, door.y2);

        if ( !room1 )
            room1 = this._make_room(door.x1, door.y1, door.x2, door.y2);

        if ( !room2 )
            room2 = this._make_room(door.x2, door.y2, door.x2, door.y2);

        if ( !room1 || !room2 )
            return false;

        if ( room1.is_djacent_to(room2) )
            return false;

        if ( room1.adjacency.length >= this.max_doors_per_room || room2.adjacency.length >= this.max_doors_per_room )
            return false;

        this.doors.push(door);
        room1.adjacency.push(new Adjacency(door, room2));
        room2.adjacency.push(new Adjacency(door, room1));
        return true;
    }

    room_at(x, y)
    {
        for ( var room of this.rooms )
            if ( room.contains(x, y) )
                return room;
        return null;
    }

    _valid_connection(from_room, x, y)
    {
        if ( x >= 0 && y >= 0 && x < this.max_x && y < this.max_y )
        {
            var room = this.room_at(x, y)
            if ( room && room.adjacency.length < this.max_doors_per_room && !room.is_djacent_to(from_room) )
                return room;
        }
        return false;
    }

    _valid_seed(x, y)
    {
        return !this.room_at(x, y) && x >= 0 && y >= 0 && x < this.max_x && y < this.max_y;
    }

    _seed_room(room)
    {
        var valid_doors = [];

        for ( var x = 0; x < room.w; x++ )
        {
            if ( this._valid_seed(room.x + x, room.y - 1) )
                valid_doors.push(new Door(room.x + x, room.y - 1, room.x + x, room.y));

            if ( this._valid_seed(room.x + x, room.y + room.h) )
                valid_doors.push(new Door(room.x + x, room.y + room.h  - 1, room.x + x, room.y + room.h));
        }

        for ( var y = 0; y < room.h; y++ )
        {
            if ( this._valid_seed(room.x - 1, room.y + y) )
                valid_doors.push(new Door(room.x - 1, room.y + y, room.x, room.y + y));

            if ( this._valid_seed(room.x + room.w, room.y + y) )
                valid_doors.push(new Door(room.x + room.w - 1, room.y + y, room.x + room.w, room.y + y));
        }

        if ( !valid_doors.length )
            return false;

        var start = this.random.rand_int(0, valid_doors.length - 1);
        for ( var i = 0; i < valid_doors.length; i++ )
        {
            if ( this._seed(valid_doors[(start+i) % valid_doors.length]) )
                return true;
        }

        return false;
    }

    _make_room(seed_x, seed_y, from_x, from_y)
    {
        var directions = Directions.Left|Directions.Right|Directions.Up|Directions.Down;

        if ( from_x < seed_x )
            directions &= ~Directions.Left;
        else if ( from_x > seed_x )
            directions &= ~Directions.Right;
        else if ( from_y > seed_y )
            directions &= ~Directions.Down;
        else if ( from_y < seed_y )
            directions &= ~Directions.Up;

        var room = new Room(seed_x, seed_y, 1, 1, this.random, "room");
        directions = this._expand(room, directions);

        while ( room.h < this.min_room_h || room.w < this.min_room_w )
        {
            if ( room.h < this.min_room_h && (directions & (Directions.Up|Directions.Down)) == 0 )
                return null;

            if ( room.w < this.min_room_w && (directions & (Directions.Right|Directions.Left)) == 0 )
                return null;

            directions = this._expand(room, directions);
        }

        this.rooms.push(room);
        this.expandable_rooms.push(room);

        return room;
    }

    _free_y(x, y, h)
    {
        if ( x < 0 || x > this.max_x || y < 0 || y + h > this.max_y )
            return false;
        for ( var i = 0; i < h; i++ )
            if ( this.room_at(x, y+i) )
                return false;
        return true;
    }

    _free_x(x, y, w)
    {
        if ( y < 0 || y > this.max_y || x < 0 || x + w > this.max_x )
            return false;
        for ( var i = 0; i < w; i++ )
            if ( this.room_at(x+i, y) )
                return false;
        return true;
    }

    _expand(room, directions)
    {
        if ( directions & Directions.Left )
        {
            var steps = this.random.rand_int(0, this.max_room_w - room.w);
            for ( var i = 0; i < steps; i++ )
            {
                if ( this._free_y(room.x - 1, room.y, room.h) )
                {
                    room.x -= 1;
                    room.w += 1;
                }
                else
                {
                    directions &= ~Directions.Left;
                    break;
                }
            }
        }

        if ( directions & Directions.Right )
        {
            var steps = this.random.rand_int(0, this.max_room_w - room.w);
            for ( var i = 0; i < steps; i++ )
            {
                if ( this._free_y(room.x + room.w + 1, room.y, room.h) )
                {
                    room.w += 1;
                }
                else
                {
                    directions &= ~Directions.Right;
                    break;
                }
            }
        }

        if ( directions & Directions.Up )
        {
            var steps = this.random.rand_int(0, this.max_room_h - room.h);
            for ( var i = 0; i < steps; i++ )
            {
                if ( this._free_x(room.x, room.y - 1, room.w) )
                {
                    room.y -= 1;
                    room.h += 1;
                }
                else
                {
                    directions &= ~Directions.Up;
                    break;
                }
            }
        }

        if ( directions & Directions.Down )
        {
            var steps = this.random.rand_int(0, this.max_room_h - room.h);
            for ( var i = 0; i < steps; i++ )
            {
                if ( this._free_x(room.x, room.y + room.h + 1, room.w) )
                {
                    room.h += 1;
                }
                else
                {
                    directions &= ~Directions.Down;
                    break;
                }
            }
        }

        return directions;
    }

    annotate_distance(from_rooms, attribute="annotation")
    {
        for ( var room of this.rooms )
            room[attribute] = null;

        var to_inspect = from_rooms;
        var distance = 0;

        while ( to_inspect.length )
        {
            var next = [];
            for ( var room of to_inspect )
            {
                if ( room[attribute] !== null )
                    continue

                room[attribute] = distance;
                for ( var adjacency of room.adjacency )
                {
                    if ( adjacency.room[attribute] === null && to_inspect.indexOf(adjacency.room) === -1 && next.indexOf(adjacency.room) === -1 )
                        next.push(adjacency.room);
                }
            }
            distance += 1;
            to_inspect = next;
        }
    }

    lock_doors(start_room, group_number, group_size)
    {
        let group_rooms = [...this.rooms_in_group(start_room, group_number)];
        let viable_rooms = group_rooms.slice();
        let biased = this.random.transformed(bias_towards_zero);

        while ( group_rooms.length < group_size && viable_rooms.length )
        {
            let index = this.random.rand_int(0, viable_rooms.length - 1);
            let room = viable_rooms[index];
            let viable_doors = room.adjacency.filter(
                adj => adj.door.group === null && group_rooms.indexOf(adj.room) === -1
            );
            if ( viable_doors.length === 0 )
            {
                viable_rooms.splice(index, 1);
                continue;
            }

            let adj = viable_doors[this.random.rand_int(0, viable_doors.length-1)];
            if ( room.group !== null && room.group < group_number )
                adj.door.group = group_number;
            else
                adj.door.group = biased.rand_int(0, group_number);
            viable_rooms.push(adj.room);
            group_rooms.push(adj.room);
        }

        for ( let room of group_rooms )
        {
            room.set_group(group_number);
            for ( let adj of room.adjacency )
            {
                if ( adj.door.group === null && adj.room.group !== null )
                {
                    if ( adj.room.group !== null && adj.room.group < group_number )
                        adj.door.group = group_number;
                    else
                        adj.door.group = biased.rand_int(0, group_number);
                }
            }
        }
    }

    rooms_in_group(start_room, group_number)
    {
        let rooms_to_process = new Set([start_room]);
        let all_rooms = new Set([start_room]);
        while ( rooms_to_process.size )
        {
            let next = new Set([]);
            for ( let room of rooms_to_process )
            {
                room.adjacency.filter(
                    e => e.door.group !== null && e.door.group <= group_number && !all_rooms.has(e.room)
                )
                .forEach(e => {next.add(e.room); all_rooms.add(e.room);})
            }
            rooms_to_process = next;
        }
        return all_rooms;
    }

    room_for_item(far_rooms, min_group=null, max_group=null)
    {
        this.annotate_distance(far_rooms, "item_distance");
        return this.rooms.filter(
            room => (min_group === null || room.group >= min_group) &&
                    (max_group === null || room.group <= max_group)
        ).reduce(
            (acc, curr) => curr.item_distance > acc.item_distance ? curr : acc
        );
    }

    apply_environments(environments)
    {
        let rooms = this.rooms.slice();
        while ( rooms.length )
        {
            let room = rooms.splice(this.random.rand_int(0, rooms.length - 1), 1)[0];
            if ( room.environment )
                continue;
            let envs = [];
            let total = 0;
            for ( let env of environments )
            {
                let prob = env.get_probability(room);
                if ( prob > 0 )
                {
                    envs.push({env: env, prob: prob});
                    total += prob;
                }
            }

            let selected_prob = this.random.random() * total;
            let selected_env = null;
            for ( let env of envs )
            {
                if ( selected_prob <= env.prob )
                {
                    selected_env = env.env;
                    break;
                }
                selected_prob -= env.prob;
            }
            if ( !selected_env )
                throw "Cannot select environment";
            room.environment = selected_env;
            selected_env.add_room(room);
        }
    }

    calculate_areas(min_size=4)
    {
        let areas = [];
        let hold = [this.rooms[0]];

        while ( hold.length )
        {

            // Get first room in hold without an area, discards those who do have one
            let next_hold = 0;
            for ( next_hold = 0; next_hold < hold.length; next_hold++ )
            {
                if ( hold[next_hold].area === null )
                    break
            }
            if ( next_hold == hold.length )
                break;
            let discard = hold.splice(0, next_hold + 1);
            let first_room = discard[discard.length - 1];

            let area = new MazeArea(first_room.environment.area_tag);
            areas.push(area);

            // Breadth search of the same area tag
            let queue = [first_room];
            while ( queue.length )
            {
                let next_queue = [];
                for ( let room of queue )
                {
                    if ( room.area )
                        continue;
                    area.rooms.push(room);
                    room.area = area;
                    for ( let adj of room.adjacency.map(a => a.room).filter(r => r.area === null) )
                    {
                        (adj.environment.area_tag == area.tag ? next_queue : hold).push(adj);
                    }
                }
                queue = next_queue;
            }
        }

        // Join small areas
        this.areas = [];
        while ( areas.length )
        {
            let area = areas.splice(0, 1)[0];
            if ( area.rooms.length >= min_size )
            {
                this.areas.push(area);
            }
            else
            {
                let neigh = area.smallest_neighbour();
                if ( neigh )
                    neigh.absorb(area);
                else
                    this.areas.push(area);
            }
        }
    }
}


class Room
{
    constructor(x, y, w, h, random)
    {
        this.x = x;
        this.y = y;
        this.w = w;
        this.h = h;
        this.random = random;
        this.adjacency = [];
        this.group = null;
        this.items = [];
        this.environment = null;
        this.area = null;
    }

    set_group(n)
    {
        this.group = this.group === null ? n : Math.min(n, this.group);
    }

    get environment_name()
    {
        return this.environment ? this.environment.name : "";
    }

    to_dom(cell_size)
    {
        var div = make_rect(
            this.x * cell_size,
            this.y * cell_size,
            this.w * cell_size - 1,
            this.h * cell_size - 1,
            this.environment_name
        );

        div.appendChild(document.createElement("div"));

        if ( this.distance !== null )
            div.appendChild(document.createElement("span")).appendChild(document.createTextNode(this.distance));

        if ( this.items.length )
        {
            for ( let item of this.items )
                div.appendChild(document.createElement("span")).appendChild(document.createTextNode(item));
        }

        if ( this.group !== null )
            div.classList.add("group"+this.group);

        return div;
    }

    contains(x, y)
    {
        return x >= this.x && x < this.x + this.w &&
               y >= this.y && y < this.y + this.h;
    }

    is_djacent_to(room)
    {
        for ( var adj of this.adjacency )
        {
            if ( adj.room == room )
                return true;
        }
        return false;
    }
}
