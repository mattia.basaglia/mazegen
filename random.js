
function bias_towards_zero(x)
{
    return 1 - Math.sqrt(1 - x**2)
}

class SeedablePRNG
{
    constructor(seed=null)
    {
        this.m = 0x80000000;
        this.a = 1103515245;
        this.c = 12345;

        if ( seed === null )
            this.randomize_seed();
        else
            this.seed(seed);
    }

    generate()
    {
        this.state = (this.a * this.state + this.c) % this.m;
        return this.state;
    }

    seed(value)
    {
        this.state = value % this.m;
        this.initial_state = this.state;
    }

    randomize_seed()
    {
        this.seed(Math.floor(Math.random() * this.m));
    }

    random()
    {
        return this.generate() / (this.m - 1);
    }

    stable_clone()
    {
        return new SeedablePRNG(this.state);
    }

    reset()
    {
        this.state = this.initial_state;
    }
}

// class DefaultPRNG
// {
//     random()
//     {
//         return Math.random();
//     }
// }

class PRNGWrapper
{
    constructor(wrapped, transform)
    {
        this.wrapped = wrapped;
        this.transform = transform;
    }

    random()
    {
        return this.transform(this.wrapped.random());
    }

    stable_clone()
    {
        return new PRNGWrapper(this.wrapped.stable_clone(), this.transform);
    }
}

class Random
{
    constructor(prng)
    {
        this.prng = prng;
    }

    /**
     * Random float in [0, 1)
     */
    random()
    {
        return this.prng.random();
    }

    /**
     * Random integer in [min, max]
     */
    rand_int(min, max)
    {
        return Math.floor(this.random() * (max - min + 1) + min);
    }

    transformed(unary_func)
    {
        return new Random(new PRNGWrapper(this.prng, unary_func));
    }

    stable_clone()
    {
        return new Random(this.prng.stable_clone());
    }
}
