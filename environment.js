class ForestEnvironment extends RoomEnvironment
{
    get name()
    {
        return "forest";
    }
}

class CaveEnvironment extends RoomEnvironment
{
    get name()
    {
        return "cave";
    }
}

class CityEnvironment extends RoomEnvironment
{
    get name()
    {
        return "city";
    }

    get area_tag()
    {
        return "urban";
    }

    _on_get_probability(room)
    {
        let prob = 0;
        for ( let adj of room.adjacency )
        {
            if ( adj.room.environment_name == "hive" )
                return 0;

            if ( adj.room.environment_name == "sewers" )
            {
                if ( adj.direction == Directions.Up )
                    return 0;

                if ( adj.direction == Directions.Down )
                    return 1;
            }

            if ( adj.room.environment_name == "city" )
            {
                if ( adj.direction & (Directions.Left|Directions.Right) )
                    prob += 2;
                else if ( adj.direction == Directions.Down )
                    prob += 1;
                else
                    prob += 0.4;
            }
        }

        return prob ? prob : -1;
    }
}

class SewersEnvironment extends RoomEnvironment
{
    get name()
    {
        return "sewers";
    }

    get area_tag()
    {
        return "urban";
    }

    _on_get_probability(room)
    {
        let prob = 0;
        for ( let adj of room.adjacency )
        {
            if ( adj.room.environment_name == "city" )
            {
                if ( adj.direction == Directions.Up )
                    prob += 2;
                else if ( adj.direction == Directions.Down )
                    return 0;
                else
                    prob += 0.3;
            }
            else if ( adj.room.environment_name == "sewers" )
            {
                if ( adj.direction & (Directions.Left|Directions.Right) )
                    prob += 2;
                else
                    prob += 0.5;
            }
        }

        return prob ? prob : -1;
    }
}

class HiveEnvironment extends RoomEnvironment
{
    get name()
    {
        return "hive";
    }

    _on_get_probability(room)
    {
        for ( let adj of room.adjacency )
        {
            if ( adj.room.environment_name == "hive" )
                return 1;
            else if ( adj.room.environment_name == "city" )
                return 0;
        }
        return -1;
    }
}


class GraveyardEnvironment extends RoomEnvironment
{
    get name()
    {
        return "graveyard";
    }

    get area_tag()
    {
        return "urban";
    }

    _on_get_probability(room)
    {
        for ( let adj of room.adjacency )
        {
            if ( adj.room.environment_name == "city" || adj.room.environment_name == "graveyard" )
            {
                if ( adj.direction & (Directions.Left|Directions.Right) )
                    return 1;
            }
        }

        return -1;
    }
}
